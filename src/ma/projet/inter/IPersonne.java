package ma.projet.inter;

public interface IPersonne {

	public String affiche();
	public double calculerSalaire();
}
