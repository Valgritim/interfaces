package ma.projet.bean;

import java.util.Date;

public class Main {

	public static void main(String[] args) {
		Profil profil = new Profil(1,123,"directeur");
		Profil profil2 = new Profil(2,456,"employ�");
		Personne pers1 = new Personne("Karim", "SAIMI", new Date("01/25/1990"), 5000, profil);
		Personne pers2 = new Personne("Eric", "Charles", new Date("03/15/1980"), 2000, profil2);
		
		System.out.println(pers1.affiche());
		System.out.println(pers2.affiche());

	}

}
