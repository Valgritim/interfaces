package ma.projet.bean;

import java.text.SimpleDateFormat;
import java.util.Date;

import ma.projet.inter.IPersonne;

public class Personne implements IPersonne{
	
	private int id;
	private static int count;
	private String firstname;
	private String lastname;
	private Date birthDate;
	private double salary;
	private Profil profil;
	
	public Personne(String firstname, String lastname, Date birthDate, double salary, Profil profil) {
		super();
		this.id = ++count;
		this.firstname = firstname;
		this.lastname = lastname;
		this.birthDate = birthDate;
		this.salary = salary;
		this.profil = profil;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public Profil getProfil() {
		return profil;
	}

	public void setProfil(Profil profil) {
		this.profil = profil;
	}


	@Override
	public String toString() {
		return "Personne [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", birthDate=" + birthDate
				+ ", salary=" + salary + ", profil=" + profil + "]";
	}
	
	public String formattageDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("d MMMM yyyy");
		String date = dateFormat.format(getBirthDate());
		return date;
	}

	@Override
	public String affiche() {
		if(profil.getLibelle().equals("employ�")) {
			return "Je suis l'" + profil.getLibelle() + " " + this.getLastname() + " " + this.getFirstname() + " n� le " + formattageDate() + ", mon salaire est " + calculerSalaire() + " dh";
			 
		 } else {
			 return "Je suis le " + profil.getLibelle() + " " + this.getLastname() + " " + this.getFirstname() + " n� le " + formattageDate() + ", mon salaire est " + calculerSalaire() + " dh";			 
		 }
		
	}

	@Override
	public double calculerSalaire() {
		 if(profil.getLibelle().equals("employ�")) {
			 salary = getSalary();
			 return salary = salary * 1.1;
			 
		 } else if (profil.getLibelle().equals("directeur")) {
			 salary = getSalary();
			 return salary = salary * 1.2;
			 
		 } else {
			 return getSalary();
		 }
		
	}
	
}
